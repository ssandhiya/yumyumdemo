Summary
=================

Photo search using the Flickr search API.

Resources
------------
Flickr API Docs: https://www.flickr.com/services/api/

Design and components used
---------------------------
Followed MVVM architecture
Used Architectural components such as LiveView, ViewModel, ViewBinding, Paging 3
Kotlin Coroutine and Coil
Integrated Flickr API's to fetch and populate the searched data in the list
Used Hilt and Dagger2 for Dependency Injection
Used Retrofit2 client for REST service

Note
----
Implemented the assessment application with and without pagination, inorder to switch between the flows , it is required to change the "startDestination" id in the nav_graph file from "search_photos_list_fragment" to "search_photos_list_paging_fragment" to have pagination flow.