package com.yumyum.demo.ui.search.paging

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.CircleCropTransformation
import com.yumyum.demo.R
import com.yumyum.demo.data.remote.search.Photo
import com.yumyum.demo.databinding.SearchPhotosListItemBinding
import com.yumyum.demo.utils.Constants.SEARCH_PHOTOS_THUMBNAIL_BASE_URL

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
class SearchPhotosListPagingAdapter(
    private val photoClickListener: (String) -> Unit,
) :
    PagingDataAdapter<Photo, SearchPhotosListPagingAdapter.ViewHolder>(PHOTO_COMPARATOR) {

    private lateinit var searchPhotosListItemBinding: SearchPhotosListItemBinding

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)

        searchPhotosListItemBinding =
            SearchPhotosListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(searchPhotosListItemBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val photo = getItem(position)
        photo?.let { viewHolder.bind(it, photoClickListener) }
    }


    inner class ViewHolder(dataBinding: SearchPhotosListItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root) {
        private val photoTitle: TextView = dataBinding.searchPhotoTextView
        private val photoImage: ImageView = dataBinding.searchPhotoImageView
        fun bind(
            photoItem: Photo,
            photoClickListener: (String) -> Unit,
        ) {
            val basePhotoURL =
                "$SEARCH_PHOTOS_THUMBNAIL_BASE_URL${photoItem.server}/${photoItem.id}_${photoItem.secret}"
            val photoURL = "${basePhotoURL}_w.jpg"

            photoImage.load(photoURL) {
                crossfade(true)
                placeholder(R.drawable.ic_placeholder_image)
                transformations(CircleCropTransformation())
            }
            photoTitle.text = photoItem.title
            itemView.setOnClickListener { photoClickListener(basePhotoURL) }
        }
    }

    companion object {
        private val PHOTO_COMPARATOR = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Photo,
                newItem: Photo
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}

