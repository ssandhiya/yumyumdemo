package com.yumyum.demo.ui.search.paging

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.PagingData
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.yumyum.demo.databinding.FragmentSearchPhotosListBinding
import com.yumyum.demo.ui.search.base.BaseNavFragment
import dagger.hilt.android.AndroidEntryPoint


/**
 * Created by srisandhiyadhanabal on 26/01/22.
 *
 * Fragment used to load search data into list using pagination concept; To use this feature, change the start destination to this fragment in the nav graph file
 */
@AndroidEntryPoint
class SearchPhotosPagingListFragment : BaseNavFragment() {

    private var _fragmentSearchPhotosListBinding: FragmentSearchPhotosListBinding? =
        null
    private val fragmentSearchPhotosListBinding get() = _fragmentSearchPhotosListBinding!!
    private val viewModel: SearchPhotosListPagingViewModel by activityViewModels()

    //region lifecycle methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {


        _fragmentSearchPhotosListBinding =
            FragmentSearchPhotosListBinding.inflate(inflater, container, false)
        return fragmentSearchPhotosListBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpLeadsListRecyclerView()
        setUpListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        _fragmentSearchPhotosListBinding = null
    }
    //endregion

    //region listener
    private fun setUpListeners() {
        fragmentSearchPhotosListBinding.searchPhotosView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                when (query.isNotEmpty()) {
                    true -> {
                        search(query.trim())
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                when (newText.isEmpty()) {
                    true -> {
                        ((fragmentSearchPhotosListBinding.searchPhotosListRecyclerView.adapter) as SearchPhotosListPagingAdapter).submitData(
                            viewLifecycleOwner.lifecycle,
                            PagingData.empty()
                        )
                        fragmentSearchPhotosListBinding.searchPhotosListRecyclerView.adapter?.notifyDataSetChanged()
                    }
                }
                return false
            }
        })
    }

    private fun onPhotoClicked(selectedPhotoURL: String) {

        val searchPhotoSelectedURL =
            SearchPhotosPagingListFragmentDirections.actionSearchPhotoDetails(
                selectedPhotoURL
            )
        findNavController().navigate(searchPhotoSelectedURL)
    }

    private fun setUpLeadsListRecyclerView() {
        val searchPhotosListAdapter = SearchPhotosListPagingAdapter { photoItem ->
            onPhotoClicked(photoItem)
        }
        with(fragmentSearchPhotosListBinding.searchPhotosListRecyclerView) {
            setHasFixedSize(true)
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    RecyclerView.VERTICAL
                )
            )
            adapter = searchPhotosListAdapter
        }

        viewModel.photos.observe(viewLifecycleOwner) {
            searchPhotosListAdapter.submitData(
                viewLifecycleOwner.lifecycle,
                it
            )
        }
    }
    //endregion

    //region functional methods
    private fun search(query: String) {
        viewModel.searchPhotos(query)
    }
    //endregion
}