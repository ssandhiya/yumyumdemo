package com.yumyum.demo.ui.search.nopaging

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.yumyum.demo.R
import com.yumyum.demo.data.remote.search.SearchPhotosResponse
import com.yumyum.demo.data.remote.search.SearchPhotosService
import com.yumyum.demo.ui.search.base.BaseViewModel
import com.yumyum.demo.utils.NetworkConnectivityManager
import com.yumyum.demo.utils.ResponseStatus
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
class SearchPhotosListViewModel @ViewModelInject constructor(
    private val networkConnectivityManager: NetworkConnectivityManager,
    @ApplicationContext private val context: Context,
) : BaseViewModel() {

    private lateinit var mSearchPhotosService: SearchPhotosService

    val searchPhotosData: MutableLiveData<ResponseStatus<SearchPhotosResponse>> = MutableLiveData()

    fun initPhotosService(searchPhotosService: SearchPhotosService) {
        mSearchPhotosService = searchPhotosService
    }

    fun getSearchedPhotos(searchText: String, pageToFetch: Int) = viewModelScope.launch {
        searchPhotosData.postValue(ResponseStatus.Loading())
        try {
            if (networkConnectivityManager.isConnectedToNetwork()) {
                val response = mSearchPhotosService.getSearchedPhotos(searchText, pageToFetch)
                searchPhotosData.postValue(handlePicsResponse(response))
            } else {
                searchPhotosData.postValue(
                    ResponseStatus.Error(
                        context.getString(
                            R.string.no_internet_connetion_error
                        )
                    )
                )
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> searchPhotosData.postValue(
                    ResponseStatus.Error(
                        context.getString(
                            R.string.network_failure_error
                        )
                    )
                )
                else -> searchPhotosData.postValue(
                    ResponseStatus.Error(
                        context.getString(
                            R.string.network_failure_error
                        )
                    )
                )
            }
        }
    }

    private fun handlePicsResponse(response: Response<SearchPhotosResponse>): ResponseStatus<SearchPhotosResponse> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return ResponseStatus.Success(resultResponse)
            }
        }
        return ResponseStatus.Error(response.message())
    }
}