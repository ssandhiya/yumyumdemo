package com.yumyum.demo.ui.search.nopaging

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.CircleCropTransformation
import com.yumyum.demo.R
import com.yumyum.demo.data.remote.search.Photo
import com.yumyum.demo.databinding.SearchPhotosListItemBinding

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
class SearchPhotosListAdapter(
    private val searchPhotosList: ArrayList<Photo>,
    private val photoClickListener: (String) -> Unit,
) :
    RecyclerView.Adapter<SearchPhotosListAdapter.ViewHolder>() {

    private lateinit var searchPhotosListItemBinding: SearchPhotosListItemBinding

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)

        searchPhotosListItemBinding =
            SearchPhotosListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(searchPhotosListItemBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val photo = searchPhotosList[position]
        viewHolder.bind(photo, photoClickListener)
    }

    override fun getItemCount(): Int {
        return searchPhotosList.size
    }

    inner class ViewHolder(dataBinding: SearchPhotosListItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root) {
        private val photoTitle: TextView = dataBinding.searchPhotoTextView
        private val photoImage: ImageView = dataBinding.searchPhotoImageView
        fun bind(
            photoItem: Photo,
            photoClickListener: (String) -> Unit,
        ) {
            val basePhotoURL =
                "https://live.staticflickr.com/${photoItem.server}/${photoItem.id}_${photoItem.secret}"
            val photoURL = "${basePhotoURL}_w.jpg"

            photoImage.load(photoURL) {
                crossfade(true)
                placeholder(R.drawable.ic_placeholder_image)
                transformations(CircleCropTransformation())
            }
            photoTitle.text = photoItem.title
            itemView.setOnClickListener { photoClickListener(basePhotoURL) }
        }
    }
}

