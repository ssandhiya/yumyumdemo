package com.yumyum.demo.ui.search.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import coil.api.load
import com.yumyum.demo.R
import com.yumyum.demo.databinding.FragmentSearchPhotoDetailsBinding
import com.yumyum.demo.ui.search.base.BaseNavFragment

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
class SearchPhotoDetailsFragment : BaseNavFragment() {
    private var _fragmentSearchPhotoDetailsBinding: FragmentSearchPhotoDetailsBinding? =
        null
    private val fragmentSearchPhotoDetailsBinding get() = _fragmentSearchPhotoDetailsBinding!!
    private val searchPhotoDetailsFragmentArgs: SearchPhotoDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _fragmentSearchPhotoDetailsBinding =
            FragmentSearchPhotoDetailsBinding.inflate(layoutInflater)


        return fragmentSearchPhotoDetailsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHomeUpEnabled(true)

        val photoURL = "${searchPhotoDetailsFragmentArgs.detailPhotoUrl}_b.jpg"
        fragmentSearchPhotoDetailsBinding.searchPhotoImageView.load(photoURL) {
            crossfade(true)
            placeholder(R.drawable.ic_placeholder_image)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _fragmentSearchPhotoDetailsBinding = null
    }

}