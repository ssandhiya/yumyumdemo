package com.yumyum.demo.ui.search.paging

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.yumyum.demo.data.remote.search.SearchPhotosRepository

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
class SearchPhotosListPagingViewModel @ViewModelInject constructor(
    private val searchPhotosRepository: SearchPhotosRepository,
    @Assisted state: SavedStateHandle
) : ViewModel() {

    private val currentQuery = state.getLiveData("searchQuery", "")

    fun searchPhotos(searchQuery: String) {
        currentQuery.value = searchQuery
    }

    val photos = currentQuery.switchMap { queryString ->
        searchPhotosRepository.getSearchedPhotos(queryString).cachedIn(viewModelScope)
    }
}