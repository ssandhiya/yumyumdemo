package com.yumyum.demo.ui.search.base

import androidx.lifecycle.ViewModel

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
open class BaseViewModel : ViewModel() {

    fun handleError(t : Throwable) {

    }
}