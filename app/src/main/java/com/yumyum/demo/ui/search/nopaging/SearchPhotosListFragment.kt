package com.yumyum.demo.ui.search.nopaging

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.yumyum.demo.data.remote.search.Photo
import com.yumyum.demo.data.remote.search.searchPhotosService
import com.yumyum.demo.databinding.FragmentSearchPhotosListBinding
import com.yumyum.demo.ui.search.base.BaseNavFragment
import com.yumyum.demo.ui.search.paging.SearchPhotosListPagingAdapter
import com.yumyum.demo.utils.ResponseStatus
import dagger.hilt.android.AndroidEntryPoint


/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
@AndroidEntryPoint
class SearchPhotosListFragment : BaseNavFragment() {

    private var _fragmentSearchPhotosListBinding: FragmentSearchPhotosListBinding? =
        null
    private val fragmentSearchPhotosListBinding get() = _fragmentSearchPhotosListBinding!!
    private val searchPhotosListViewModel: SearchPhotosListViewModel by activityViewModels()
    private var searchPhotosListItems = arrayListOf<Photo>()

    //region lifecycle methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {


        _fragmentSearchPhotosListBinding =
            FragmentSearchPhotosListBinding.inflate(inflater, container, false)
        return fragmentSearchPhotosListBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpLeadsListRecyclerView()
        setUpListeners()
        setUpObserver()

        searchPhotosListViewModel.initPhotosService(
            searchPhotosService()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        _fragmentSearchPhotosListBinding = null
    }
    //endregion

    //region listener
    private fun setUpListeners() {
        fragmentSearchPhotosListBinding.searchPhotosView.onActionViewExpanded()
        fragmentSearchPhotosListBinding.searchPhotosView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                when (query.isNotEmpty()) {
                    true -> {
                        searchPhotosListViewModel.getSearchedPhotos(query.trim(), 1)
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                when (newText.isEmpty()) {
                    true -> {
                        searchPhotosListItems.clear()
                        fragmentSearchPhotosListBinding.searchPhotosListRecyclerView.adapter?.notifyDataSetChanged()
                    }
                }
                return false
            }
        })
    }

    private fun onPhotoClicked(selectedPhotoURL: String) {

        val searchPhotoSelectedURL =
            SearchPhotosListFragmentDirections.actionSearchPhotoDetails(
                selectedPhotoURL
            )
        findNavController().navigate(searchPhotoSelectedURL)
    }

    private fun setUpLeadsListRecyclerView() {
        val searchPhotosListAdapter = SearchPhotosListPagingAdapter { photoItem ->
            onPhotoClicked(photoItem)
        }
        with(fragmentSearchPhotosListBinding.searchPhotosListRecyclerView) {
            setHasFixedSize(true)
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    RecyclerView.VERTICAL
                )
            )

            adapter =
                SearchPhotosListAdapter(
                    searchPhotosListItems
                ) { photoItem ->
                    onPhotoClicked(photoItem)
                }
        }
    }
    //endregion

    //region observer
    private fun setUpObserver() {
        searchPhotosListViewModel.searchPhotosData.observe(viewLifecycleOwner, {
            when (it) {
                is ResponseStatus.Success -> {
                    fragmentSearchPhotosListBinding.searchPhotosSpinner.progressbar.visibility =
                        GONE
                    it.data?.photos?.photo?.populateListData()
                }
                is ResponseStatus.Loading ->
                    fragmentSearchPhotosListBinding.searchPhotosSpinner.progressbar.visibility =
                        VISIBLE

                is ResponseStatus.Error -> {
                    Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                    fragmentSearchPhotosListBinding.searchPhotosSpinner.progressbar.visibility =
                        GONE
                }
            }
        })
    }
    //endregion

    //region functional methods
    private fun List<Photo>.populateListData() {
        when (size > 0) {
            true -> {
                fragmentSearchPhotosListBinding.searchPhotosListNoDataView.visibility = GONE
                searchPhotosListItems.addAll(this)
                fragmentSearchPhotosListBinding.searchPhotosListRecyclerView.adapter?.notifyDataSetChanged()
            }
            false -> fragmentSearchPhotosListBinding.searchPhotosListNoDataView.visibility =
                VISIBLE
        }
    }
    //endregion
}