package com.yumyum.demo.ui.search.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.yumyum.demo.R

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
open class BaseNavFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.apply {
            setBackgroundDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.ic_toolbar_bg
                )
            })
            setHomeUpEnabled(false)
        }
    }

    fun setHomeUpEnabled(isUpEnabled: Boolean) {
        (activity as AppCompatActivity).supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(isUpEnabled)
        }
    }
}