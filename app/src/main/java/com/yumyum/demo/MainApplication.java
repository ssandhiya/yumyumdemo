package com.yumyum.demo;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
@HiltAndroidApp
public class MainApplication extends Application {
}
