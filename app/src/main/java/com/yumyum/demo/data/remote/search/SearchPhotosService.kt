package com.yumyum.demo.data.remote.search


import com.yumyum.demo.data.remote.BaseService
import com.yumyum.demo.utils.Constants
import com.yumyum.demo.utils.Constants.SEARCH_API_KEY
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
interface SearchPhotosService {

    @GET("?method=flickr.photos.search&per_page=25&format=json&nojsoncallback=1&api_key=$SEARCH_API_KEY")
    suspend fun getSearchedPhotos(
        @Query(value = "text") searchTerm: String,
        @Query(value = "page") pageToFetch: Int
    ): Response<SearchPhotosResponse>
}

fun searchPhotosService(): SearchPhotosService {
    return BaseService.getApiService(Constants.SEARCH_PHOTOS_BASE_URL)
        .create(SearchPhotosService::class.java)
}