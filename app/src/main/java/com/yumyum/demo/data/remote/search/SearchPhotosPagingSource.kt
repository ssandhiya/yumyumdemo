package com.yumyum.demo.data.remote.search

import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import java.io.IOException

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
private const val STARTING_PAGE_INDEX = 1

class SearchPhotosPagingSource(
    private val searchPhotosService: SearchPhotosPagingService,
    private val query: String
) : PagingSource<Int, Photo>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Photo> {
        val pageToFetch = params.key ?: STARTING_PAGE_INDEX

        return try {
            val response = searchPhotosService.getSearchedPhotos(query, pageToFetch)
            val photosList = response.body()?.photos?.photo ?: emptyList()

            LoadResult.Page(
                data = photosList,
                prevKey = if (pageToFetch == STARTING_PAGE_INDEX) null else pageToFetch - 1,
                nextKey = if (photosList.isEmpty()) null else pageToFetch + 1
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Photo>): Int? {
        TODO("Not yet implemented")
    }
}
