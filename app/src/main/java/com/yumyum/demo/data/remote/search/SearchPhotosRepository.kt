package com.yumyum.demo.data.remote.search

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
@Singleton
class SearchPhotosRepository @Inject constructor(private val searchPhotosService: SearchPhotosPagingService) {

    fun getSearchedPhotos(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 25,
                maxSize = 120,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { SearchPhotosPagingSource(searchPhotosService, query) }
        ).liveData
}