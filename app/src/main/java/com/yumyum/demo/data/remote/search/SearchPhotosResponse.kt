package com.yumyum.demo.data.remote.search

import com.google.gson.annotations.SerializedName

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
data class SearchPhotosResponse(
    @SerializedName("photos")
    val photos: Photos
)

data class Photos(
    @SerializedName("page")
    val page: Int,

    @SerializedName("pages")
    val pages: String,

    @SerializedName("perpage")
    val perpage: Int,

    @SerializedName("total")
    val total: String,

    @SerializedName("photo")
    val photo: List<Photo>
)

data class Photo(
    @SerializedName("id")
    val id: String,

    @SerializedName("owner")
    val owner: String,

    @SerializedName("secret")
    val secret: String,

    @SerializedName("server")
    val server: String,

    @SerializedName("farm")
    val farm: String,

    @SerializedName("title")
    val title: String
)
