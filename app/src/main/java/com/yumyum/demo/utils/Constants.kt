package com.yumyum.demo.utils

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
object Constants {
        const val SEARCH_PHOTOS_BASE_URL = "https://www.flickr.com/services/rest/"
        const val SEARCH_API_KEY = "1508443e49213ff84d566777dc211f2a"
        const val SEARCH_PHOTOS_THUMBNAIL_BASE_URL = "https://live.staticflickr.com/"
}