package com.yumyum.demo.di


import com.yumyum.demo.data.remote.search.SearchPhotosPagingService
import com.yumyum.demo.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by srisandhiyadhanabal on 26/01/22.
 */
@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(Constants.SEARCH_PHOTOS_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideSearchPhotoService(retrofit: Retrofit): SearchPhotosPagingService =
        retrofit.create(SearchPhotosPagingService::class.java)
}